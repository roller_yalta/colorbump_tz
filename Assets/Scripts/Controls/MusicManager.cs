﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{    
    [SerializeField] private AudioSource music;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void OnEnable()
    {
        Utils.MusicSwitch.AddListener(OnMusicSwitch);
    }

    private void OnDisable()
    {
        Utils.MusicSwitch.RemoveListener(OnMusicSwitch);
    }

    private void OnMusicSwitch(bool isOn)
    {
        music.volume = isOn ? 1f : 0f;
    }
}