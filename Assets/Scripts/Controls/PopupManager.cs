﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    [SerializeField] private LevelEndPanel levelEndPopup;

    private void OnEnable()
    {
        Utils.EndGame.AddListener(OnEndGame);
    }

    private void OnDisable()
    {
        Utils.EndGame.RemoveListener(OnEndGame);
    }

    private void OnEndGame(bool result)
    {
        var endPanel = Instantiate(levelEndPopup, transform);
        endPanel.SetHeaderText(result ? Utils.WIN_HEADER : Utils.LOOSE_HEADER);
    }
}