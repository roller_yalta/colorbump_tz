﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ShellLoader : MonoBehaviour
{
    public void Awake()
    {
        SceneManager.LoadScene((int) Utils.SceneInfo.Menu);
    }
}