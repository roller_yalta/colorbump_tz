﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class ObstaclesGenerator : MonoBehaviour
{
    [SerializeField] private GameObject[] neutrals;
    [SerializeField] private GameObject[] obstacles;

    [Header("matrix values")] [SerializeField]
    private float randomFactor;

    [SerializeField] private int matrixX;
    [SerializeField] private int matrixY;

    [Header("grid objects offset")] [SerializeField]
    private float xOffset;

    [SerializeField] private float zOffset;

    private int[,] matrix;

    private void Awake()
    {
        GenerateLevel();
    }

    private void GenerateLevel()
    {
        matrix = new int[matrixX, matrixY];
        for (var i = 0; i < matrixX; i++)
        {
            for (var j = 0; j < matrixY; j++)
            {
                matrix[i, j] = Random.value > randomFactor ? 1 : 0;
                var prefab = matrix[i, j] == 1
                    ? neutrals[Random.Range(0, obstacles.Length)]
                    : obstacles[Random.Range(0, obstacles.Length)];
                var position = new float3(i * xOffset, 1, j * zOffset); //y is freezed
                var obj = Instantiate(prefab, Vector3.zero, Quaternion.identity);
                obj.transform.SetParent(transform);
                obj.transform.localPosition = position;
            }
        }
    }
}