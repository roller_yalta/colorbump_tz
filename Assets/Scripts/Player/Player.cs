﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [Header("Player parameters")] 
    [SerializeField] private float ballSpeed;
    [SerializeField] private float camSpeed;
    [SerializeField] private float edtiorBallAcceleration;
    [SerializeField] private float deviceBallAcceleration;
    [SerializeField] private float lerpSpeed;
    [SerializeField] private Rigidbody rgbd;
    [SerializeField] private float minCamToPlayer;
    [SerializeField] private float maxCamToPlayer;

    private Camera camera;
    private Transform cachedTr;
    private float cameraToPlayerDistance;
    private readonly string obstacleTag = "Obstacle";
    private readonly string finishTag = "Finish";
    private float ballAcceleration = 0f;

    private void Awake()
    {
        cachedTr = transform;
        camera = Camera.main;
#if UNITY_ANDROID && !UNITY_EDITOR
        ballAcceleration = deviceBallAcceleration;
#else
        ballAcceleration = edtiorBallAcceleration;
#endif
    }

    private void Update()
    {
        if (Input.GetButton("Fire1"))
            Move(Input.mousePosition);
        CameraGapComputing();
    }

    private void FixedUpdate()
    {
        PhysMove();
    }

    private void Move(Vector3 pos)
    {
        var r = camera.ScreenPointToRay(pos);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, 100))
        {
            cachedTr.position = Vector3.Lerp(cachedTr.position,
                new float3(hit.point.x, cachedTr.position.y, cachedTr.position.z), lerpSpeed);
        }
    }

    private void PhysMove()
    {
        if (cameraToPlayerDistance < maxCamToPlayer && cameraToPlayerDistance > minCamToPlayer)
        {
            rgbd.velocity = Vector3.forward * ballSpeed;
            if (Input.GetButton("Fire1"))
            {
                var pointerY = Input.GetAxis("Mouse Y");
                if (Input.touchCount > 0)
                    pointerY = Input.touches[0].deltaPosition.y;
                var acceleration = pointerY * ballAcceleration;
                rgbd.velocity = Vector3.forward * (ballSpeed + acceleration);
            }
        }
        else
        {
            rgbd.velocity = Vector3.forward * camSpeed;
            if (Input.GetButton("Fire1"))
            {
                var pointerY = Input.GetAxis("Mouse Y");
                if (Input.touchCount > 0)
                    pointerY = Input.touches[0].deltaPosition.y;
                var acceleration = pointerY * ballAcceleration;
                if (cameraToPlayerDistance >= maxCamToPlayer)
                    if (acceleration < 0)
                        rgbd.velocity = Vector3.forward * (ballSpeed + acceleration);
                if (cameraToPlayerDistance <= minCamToPlayer)
                    if (acceleration > 0)
                        rgbd.velocity = Vector3.forward * (ballSpeed + acceleration);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(finishTag))
        {
            Utils.EndGame.Invoke(true);
            DisablePlayer();
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.rigidbody.CompareTag(obstacleTag))
        {
            Utils.EndGame.Invoke(false);
            DisablePlayer();
        }
    }

    private void CameraGapComputing()
    {
        cameraToPlayerDistance = Vector3.Distance(new Vector3(0, 0, camera.transform.position.z),
            new Vector3(0, 0, cachedTr.position.z));
    }

    private void DisablePlayer()
    {
        //or disable collider
        gameObject.SetActive(false);
    }
}