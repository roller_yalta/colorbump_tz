﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelEndPanel : MonoBehaviour
{
    [SerializeField] private Text header;

    public void Replay()
    {
        SceneManager.LoadScene((int)Utils.SceneInfo.Game);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene((int)Utils.SceneInfo.Menu);
    }

    public void SetHeaderText(string text)
    {
        header.text = text;
    }
}
