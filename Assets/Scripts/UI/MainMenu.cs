﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{   
    public void OnPlayButton()
    {
        SceneManager.LoadScene((int) Utils.SceneInfo.Game);
    }

    public void OnSettingsButton()
    {
        SceneManager.LoadScene((int) Utils.SceneInfo.Settings);
    }
}
