﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [SerializeField] private Button musicOn;
    [SerializeField] private Button musicOff;

    public void OnMusicControl(bool isOn)
    {
        Utils.MusicSwitch.Invoke(isOn);
        musicOff.image.sprite = isOn ? musicOff.spriteState.disabledSprite : musicOff.spriteState.pressedSprite;
        musicOn.image.sprite = isOn ? musicOff.spriteState.pressedSprite : musicOff.spriteState.disabledSprite;
    }

    public void OnBackButton()
    {
        SceneManager.LoadScene((int) Utils.SceneInfo.Menu);
    }
}