﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;   

public class CameraMover : MonoBehaviour
{
   [SerializeField] private Rigidbody rgbd;
   [SerializeField] private float cameraSpeed;

   private void FixedUpdate()
   {
      rgbd.velocity = Vector3.forward * cameraSpeed;
   }
}
