﻿using System;
using UnityEngine.Events;

public class Utils
{
    public const string WIN_HEADER = "You Win!";
    public const string LOOSE_HEADER = "You Lose :(";
    
    public enum SceneInfo
    {
        Shell,
        Menu,
        Game,
        Settings
    }

    public class EndGameEvent : UnityEvent<bool>
    {
    }

    public class MusicSwitchEvent : UnityEvent<bool>
    {
    }

    public static readonly EndGameEvent EndGame = new EndGameEvent();
    public static readonly MusicSwitchEvent MusicSwitch = new MusicSwitchEvent();
}
